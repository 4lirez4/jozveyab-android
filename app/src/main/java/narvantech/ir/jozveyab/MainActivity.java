package narvantech.ir.jozveyab;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import narvantech.ir.jozveyab.addnote.AddNoteActivity;
import narvantech.ir.jozveyab.allnotes.AllNoteAdapter;
import narvantech.ir.jozveyab.allnotes.AllNotesModel;
import narvantech.ir.jozveyab.login.SignInActivity;
import narvantech.ir.jozveyab.models.Jozve;
import narvantech.ir.jozveyab.professor.ProfessorListActivity;
import narvantech.ir.jozveyab.utils.BaseActivity;
import narvantech.ir.jozveyab.utils.Constants;
import narvantech.ir.jozveyab.utils.Ret;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    RecyclerView rv_notes;
    List<AllNotesModel> noteList;
    AllNoteAdapter adapter;
    TextView username;
    TextView useremail;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        rv_notes = findViewById(R.id.rv_notes);
//        prepareData();




        username = navigationView.getHeaderView(0).findViewById(R.id.user_name_txt);
        username.setText(Constants.Username);
        useremail = navigationView.getHeaderView(0).findViewById(R.id.user_email_txt);
        useremail.setText(Constants.Useremail);


    }

    @Override
    protected void onResume() {
        super.onResume();
        GetNoteRet ret=new GetNoteRet();
        ret.start();

    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_add) {
            Intent intent = new Intent(MainActivity.this, AddNoteActivity.class);
            startActivity(intent);
            // Handle the camera action
        } else if (id == R.id.nav_my_lib) {

            setTitle("تست");
            Intent intent = new Intent(MainActivity.this, TestActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(MainActivity.this, ProfessorListActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_category) {
            Intent intent = new Intent(MainActivity.this, CatgoryActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_login) {
            Intent intent = new Intent(MainActivity.this, SignInActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_send) {
            Intent intent = new Intent(MainActivity.this, AboutActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private class GetNoteRet implements Callback<List<Jozve>> {
        public List<Jozve> myData;

        public void start() {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            Ret ret = retrofit.create(Ret.class);
            Call<List<Jozve>> call = ret.getAllNote();
            call.enqueue(this);
        }

        @Override
        public void onResponse(Call<List<Jozve>> call, Response<List<Jozve>> response) {
            if (response.isSuccessful()) {
                this.myData = response.body();
                Log.d("server", response.raw().message());
//                Toast.makeText(getApplicationContext(), "شما با موفقیت وارد شدید", Toast.LENGTH_SHORT).show();
                adapter = new AllNoteAdapter(myData,getApplicationContext());
                rv_notes.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                rv_notes.setItemAnimator(new DefaultItemAnimator());
                rv_notes.setAdapter(adapter);

            }
        }

        @Override
        public void onFailure(Call<List<Jozve>> call, Throwable t) {
            Log.d("server", t.getMessage());

            Toast.makeText(getApplicationContext(), "خطا در برقراری ارتباط با سرور", Toast.LENGTH_SHORT).show();

        }
    }


}
