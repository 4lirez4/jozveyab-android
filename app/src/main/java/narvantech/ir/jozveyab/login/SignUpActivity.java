package narvantech.ir.jozveyab.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import narvantech.ir.jozveyab.R;
import narvantech.ir.jozveyab.utils.BaseActivity;
import narvantech.ir.jozveyab.utils.Constants;
import narvantech.ir.jozveyab.utils.Ret;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SignUpActivity extends BaseActivity {

    TextView link_login;
    EditText fName;
    EditText lName;
    EditText email;
    EditText password;
    Button btn_signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        fName = findViewById(R.id.input_fname);
        lName = findViewById(R.id.input_lname);
        email = findViewById(R.id.input_email);
        password = findViewById(R.id.input_password);
        btn_signup = findViewById(R.id.btn_signup);
        final Intent intent = new Intent(SignUpActivity.this, SignInActivity.class);
        link_login = findViewById(R.id.link_login);
        link_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(intent);
            }
        });

        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    SignUpRet signUpRet = new SignUpRet();
                    signUpRet.start(fName.getText().toString(), lName.getText().toString()
                            , email.getText().toString(), password.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    public void onBackPressed() {
        finish();
    }


    private class SignUpRet implements Callback<ResponseBody> {
        public ResponseBody myData;

        public void start(String fname, String lname, String email, String password) {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            Ret ret = retrofit.create(Ret.class);
            Call<ResponseBody> call = ret.signUp(fname, lname, email, password);
            call.enqueue(this);
        }

        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            if (response.isSuccessful()) {
                this.myData = response.body();
                Toast.makeText(getApplicationContext(), "شما با موفقیت ثبتنام شدید", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t) {

            Toast.makeText(getApplicationContext(), "مشکل", Toast.LENGTH_SHORT).show();

        }
    }


}
