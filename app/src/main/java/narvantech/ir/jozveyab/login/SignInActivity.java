package narvantech.ir.jozveyab.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import narvantech.ir.jozveyab.MainActivity;
import narvantech.ir.jozveyab.R;
import narvantech.ir.jozveyab.models.Users;
import narvantech.ir.jozveyab.utils.BaseActivity;
import narvantech.ir.jozveyab.utils.Constants;
import narvantech.ir.jozveyab.utils.Ret;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SignInActivity extends BaseActivity {

    TextView Create_Account;
    EditText email;
    EditText password;

    ImageView logo;
    Button btn_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = findViewById(R.id.input_email);
        password = findViewById(R.id.input_password);
        btn_login = findViewById(R.id.btn_login);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        final Intent intent=new Intent(SignInActivity.this,SignUpActivity.class);
        Create_Account =findViewById(R.id.link_signup);
        Create_Account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 startActivity(intent);
            }
        });
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SignInRet sign=new SignInRet();
                sign.start(email.getText().toString(), password.getText().toString());

            }
        });

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }


    private class SignInRet implements Callback<Users> {
        public Users myData;

        public void start( String email, String password) {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            Ret ret = retrofit.create(Ret.class);
            Call<Users> call = ret.signIn( email, password);
            call.enqueue(this);
        }

        @Override
        public void onResponse(Call<Users> call, Response<Users> response) {
                Log.d("signin", response.raw().message());
            if (response.isSuccessful()) {
                this.myData = response.body();
                if (myData.getId() == 0) {
                    Toast.makeText(getApplicationContext(), "چنین کاربری وجود ندارد !!", Toast.LENGTH_SHORT).show();
                } else {
                Toast.makeText(getApplicationContext(), "خوش آمدید" +myData.getFname(), Toast.LENGTH_SHORT).show();
                Constants.Useremail=myData.getEmail();
                Constants.Username=myData.getFname()+myData.getLname();
                    Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            }
        }

        @Override
        public void onFailure(Call<Users> call, Throwable t) {

            Toast.makeText(getApplicationContext(), "مشکل", Toast.LENGTH_SHORT).show();

        }
    }



}
