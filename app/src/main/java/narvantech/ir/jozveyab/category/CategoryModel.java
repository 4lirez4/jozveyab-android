package narvantech.ir.jozveyab.category;

/**
 * Created by Ho3ein on 3/4/2018.
 */

public class CategoryModel {
    private String name;
    private int id;
    private int img_link;

    public CategoryModel(int id,String name, int img_link) {
        this.name = name;
        this.img_link = img_link;
        this.id=id;
    }


    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getImg_link() {
        return img_link;
    }

    public void setImg_link(int img_link) {
        this.img_link = img_link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
