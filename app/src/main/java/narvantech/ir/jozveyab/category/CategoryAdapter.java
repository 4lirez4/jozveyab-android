package narvantech.ir.jozveyab.category;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import narvantech.ir.jozveyab.R;
import narvantech.ir.jozveyab.allnotes.AllNotesModel;
import narvantech.ir.jozveyab.allnotes.CourseNoteList;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.NoteViewHolder> {

    private List<CategoryModel> notes;
    private Context context;

    public CategoryAdapter(List<CategoryModel> notes, Context context) {
        //    if(this.notes==null){
        //       this.notes=new ArrayList<AllNotesModel>();
        //     }else {
        this.notes = notes;
        this.context = context;
        //    }
    }

    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemview = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cat_list_item, parent, false);
        return new NoteViewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(NoteViewHolder holder, final int position) {
        holder.bind(notes.get(position));
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CourseNoteList.class);
                intent.putExtra("id", notes.get(position).getId());
                intent.putExtra("name", notes.get(position).getName());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    static class NoteViewHolder extends RecyclerView.ViewHolder {

        private TextView title;
        private ImageView image;

        //   private TextView tvDate;
        //   private RatingBar ratingBar;
        //  private ImageView imgNote;

        public NoteViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.cat_title);
            image = itemView.findViewById(R.id.img_category);
            //       tvDate =itemView.findViewById(R.id.tv_date_note_list);
            //       ratingBar=itemView.findViewById(R.id.ratingbar_list);
            //       imgNote=itemView.findViewById(R.id.img_note_list);
        }

        public void bind(CategoryModel cm) {
            title.setText(cm.getName());
            image.setImageResource(cm.getImg_link());
            //    tvDate.setText(note.getDate());

        }
    }
}