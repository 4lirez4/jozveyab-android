package narvantech.ir.jozveyab.addnote;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import narvantech.ir.jozveyab.R;
import narvantech.ir.jozveyab.utils.BaseActivity;
import narvantech.ir.jozveyab.utils.Constants;
import narvantech.ir.jozveyab.utils.Ret;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddNoteActivity extends BaseActivity {

    EditText name;
    EditText description;
    Button send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        name = findViewById(R.id.addnote_name);
        description = findViewById(R.id.addnote_desc);
        send = findViewById(R.id.addnote_send);

        description.setMovementMethod(new ScrollingMovementMethod());


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PostNote postNote = new PostNote();
                postNote.start(name.getText().toString(), "test", description.getText().toString());
            }
        });


    }


    private class PostNote implements Callback<ResponseBody> {
        public ResponseBody myData;

        public void start(String name, String link, String description) {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            Ret ret = retrofit.create(Ret.class);
            Call<ResponseBody> call = ret.postNote(name, link, description);
            call.enqueue(this);
        }

        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            if (response.isSuccessful()) {
                this.myData = response.body();
                Toast.makeText(getApplicationContext(), "جزوه با موفقیت ثبت شد", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t) {

            Toast.makeText(getApplicationContext(), "مشکل", Toast.LENGTH_SHORT).show();

        }
    }


}
