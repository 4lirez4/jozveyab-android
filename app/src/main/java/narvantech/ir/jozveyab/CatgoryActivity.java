package narvantech.ir.jozveyab;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import narvantech.ir.jozveyab.category.CategoryAdapter;
import narvantech.ir.jozveyab.category.CategoryModel;
import narvantech.ir.jozveyab.utils.BaseActivity;

public class CatgoryActivity extends BaseActivity {
    RecyclerView recyclerView;
    List<CategoryModel> catlist;
    CategoryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catgory);
        recyclerView = findViewById(R.id.category_recyclerview);
        prepareData();
        adapter = new CategoryAdapter(catlist,getApplicationContext());
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void prepareData() {
        if (catlist == null) {
            catlist = new ArrayList<CategoryModel>();
        } else {
            catlist.clear();
        }
        catlist.add(new CategoryModel(1, "کامپیوتر", R.drawable.cat_back1));
        catlist.add(new CategoryModel(2, "معماری", R.drawable.cat_back2));
        catlist.add(new CategoryModel(3, "مکانیک", R.drawable.cat_back3));
        catlist.add(new CategoryModel(4, "برق", R.drawable.cat_back3));
        // catlist.add(new CategoryModel( "گرافیک",R.drawable.cat_back4));
        //   catlist.add(new CategoryModel( "عمران",R.drawable.cat_back5));
        //  catlist.add(new CategoryModel( "برق",R.drawable.cat_back6));
     /*   catlist.add(new CategoryModel( "زبان",R.drawable.cat_back7));
        catlist.add(new CategoryModel( "چوب",R.drawable.cat_back8));   */
    }
}

