package narvantech.ir.jozveyab.welcom;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import narvantech.ir.jozveyab.MainActivity;
import narvantech.ir.jozveyab.R;
import narvantech.ir.jozveyab.*;

public class SplashActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        final Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new CountDownTimer(2000, 1000) {
            @Override
            public void onTick(long l) {
            }
            @Override
            public void onFinish() {
                SplashActivity.this.startActivity(intent);
            }



            }.start();

        }

    @Override
    protected void onRestart() {
        SplashActivity.this.finish();
        super.onRestart();
    }
}
