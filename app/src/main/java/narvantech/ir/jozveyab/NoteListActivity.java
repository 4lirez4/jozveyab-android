package narvantech.ir.jozveyab;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import narvantech.ir.jozveyab.allnotes.AllNoteAdapter;
import narvantech.ir.jozveyab.allnotes.AllNotesModel;
import narvantech.ir.jozveyab.allnotes.OneNote;
import narvantech.ir.jozveyab.utils.BaseActivity;

public class NoteListActivity extends BaseActivity {

    RecyclerView rv_notes;
    List<AllNotesModel> noteList;
    AllNoteAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notelist);

        rv_notes=findViewById(R.id.rv_notes);
        prepareData();

//        adapter=new AllNoteAdapter(noteList);
        rv_notes.setLayoutManager(new LinearLayoutManager(this));
        rv_notes.setItemAnimator(new DefaultItemAnimator());
        rv_notes.setAdapter(adapter);


    }

    private void prepareData() {
        if(noteList==null){
            noteList=new ArrayList<AllNotesModel>();
        } else{
            noteList.clear();
        }

         noteList.add(new AllNotesModel("نام","توضیح","رشته","دانشگاه"));
         noteList.add(new AllNotesModel("نام","توضیح","رشته","دانشگاه"));
         noteList.add(new AllNotesModel("نام","توضیح","رشته","دانشگاه"));
         noteList.add(new AllNotesModel("نام","توضیح","رشته","دانشگاه"));
         noteList.add(new AllNotesModel("نام","توضیح","رشته","دانشگاه"));


    }
}
