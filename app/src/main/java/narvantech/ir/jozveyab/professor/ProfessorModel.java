package narvantech.ir.jozveyab.professor;

/**
 * Created by Ho3ein on 2/28/2018.
 */

public class ProfessorModel {

    private int id;
    private String name;
    private String college;
    private String university;
    private String degree;
    private String wiki;
    private String image;

    ProfessorModel(String name, String college){

      //  this.id = id;
        this.name = name;
        this.college = college;
      //  this.university = university;
     //   this.degree = degree;
      //  this.wiki = wiki;
      //  this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getWiki() {
        return wiki;
    }

    public void setWiki(String wiki) {
        this.wiki = wiki;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }
}
