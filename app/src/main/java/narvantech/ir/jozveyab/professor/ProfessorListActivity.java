package narvantech.ir.jozveyab.professor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import narvantech.ir.jozveyab.R;
import narvantech.ir.jozveyab.allnotes.AllNoteAdapter;
import narvantech.ir.jozveyab.allnotes.AllNotesModel;

public class ProfessorListActivity extends AppCompatActivity {

    RecyclerView rv_professor;
    List<ProfessorModel> ProfessorsList;
    ProfessorAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_professor_list);


        rv_professor=findViewById(R.id.rv_professor);
        prepareData();

        adapter=new ProfessorAdapter(ProfessorsList);
        rv_professor.setLayoutManager(new LinearLayoutManager(this));
        rv_professor.setItemAnimator(new DefaultItemAnimator());
        rv_professor.setAdapter(adapter);


    }

    private void prepareData() {
        if(ProfessorsList==null){
            ProfessorsList=new ArrayList<ProfessorModel>();
        } else{
            ProfessorsList.clear();
        }

        ProfessorsList.add(new ProfessorModel("علی نوراله","دانشکده کامپیوتر"));
        ProfessorsList.add(new ProfessorModel("سحر کیانیان","دانشکده کامپیوتر"));
        ProfessorsList.add(new ProfessorModel("نگین دانشپور","دانشکده کامپیوتر"));
        ProfessorsList.add(new ProfessorModel("علیرضا بساق زاده","دانشکده کامپیوتر"));
        ProfessorsList.add(new ProfessorModel("صدیقه اسکندری راد","دانشکده علوم پایه"));
        ProfessorsList.add(new ProfessorModel("علیرضا رضایی","دانشکده کامپیوتر"));
        ProfessorsList.add(new ProfessorModel("محمد چشمفر","دانشکده برق"));
        ProfessorsList.add(new ProfessorModel("یعقوب دادگر اصل","دانشکده مکانیک"));
        ProfessorsList.add(new ProfessorModel("ولی الله پناهی زاده","دانشکده مکانیک"));
        ProfessorsList.add(new ProfessorModel("فرامرز آشنای قاسمی","دانشکده مکانیک"));

    }
    }

