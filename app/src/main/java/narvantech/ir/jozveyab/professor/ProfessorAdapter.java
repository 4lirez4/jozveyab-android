package narvantech.ir.jozveyab.professor;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import narvantech.ir.jozveyab.R;
import narvantech.ir.jozveyab.allnotes.AllNoteAdapter;
import narvantech.ir.jozveyab.allnotes.AllNotesModel;

/**
 * Created by Ho3ein on 2/28/2018.
 */

public class ProfessorAdapter extends RecyclerView.Adapter<ProfessorAdapter.Professorviewholder>{

    private List<ProfessorModel> ProfessorList;
    ProfessorAdapter(List<ProfessorModel> ProfessorList){
        this.ProfessorList=ProfessorList;
    }

    @Override
    public ProfessorAdapter.Professorviewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemview= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_professor_list,parent,false);
        return new ProfessorAdapter.Professorviewholder(itemview);
    }

    @Override
    public void onBindViewHolder(ProfessorAdapter.Professorviewholder holder, int position) {
        holder.bind(ProfessorList.get(position));
    }

    @Override
    public int getItemCount() {
        return ProfessorList.size();
    }

    static class Professorviewholder extends RecyclerView.ViewHolder{


        private TextView tvName;
        private TextView tvCollege;


        public Professorviewholder(View itemView) {
            super(itemView);
            tvName =itemView.findViewById(R.id.tv_name_professor);
            tvCollege =itemView.findViewById(R.id.tv_college_professor);
        }
        public void bind(ProfessorModel professor){
            tvName.setText(professor.getName());
            tvCollege.setText(professor.getCollege());
        }
    }



}
