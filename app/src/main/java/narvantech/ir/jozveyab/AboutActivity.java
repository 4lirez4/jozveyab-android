package narvantech.ir.jozveyab;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import narvantech.ir.jozveyab.utils.BaseActivity;

public class AboutActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}
