package narvantech.ir.jozveyab.updatenote;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import narvantech.ir.jozveyab.MainActivity;
import narvantech.ir.jozveyab.R;
import narvantech.ir.jozveyab.utils.BaseActivity;
import narvantech.ir.jozveyab.utils.Constants;
import narvantech.ir.jozveyab.utils.Ret;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class UpdateNoteActivity extends BaseActivity {

    EditText name;
    EditText description;
    Button send;
    public int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        name = findViewById(R.id.addnote_name);
        description = findViewById(R.id.addnote_desc);
        send = findViewById(R.id.addnote_send);

        id = getIntent().getIntExtra("id", 1);
        name.setText(getIntent().getStringExtra("name"));
        description.setText(getIntent().getStringExtra("desc"));
        description.setMovementMethod(new ScrollingMovementMethod());

        send.setText("به روز رسانی");
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateNote updateNote = new UpdateNote();
                updateNote.start(id, name.getText().toString(), description.getText().toString());
            }
        });


    }




    private class UpdateNote implements Callback<ResponseBody> {
        public ResponseBody myData;

        public void start(int id, String name, String description) {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            Ret ret = retrofit.create(Ret.class);
            Call<ResponseBody> call = ret.updateNote(id, name, description);
            call.enqueue(this);
        }

        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            if (response.isSuccessful()) {
                this.myData = response.body();
                Toast.makeText(getApplicationContext(), "جزوه با موفقیت بروز شد", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(UpdateNoteActivity.this, MainActivity.class));
            }
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t) {

            Toast.makeText(getApplicationContext(), "مشکل", Toast.LENGTH_SHORT).show();

        }
    }


}
