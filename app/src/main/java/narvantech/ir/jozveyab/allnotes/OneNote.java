package narvantech.ir.jozveyab.allnotes;
import narvantech.ir.jozveyab.R;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;

public class OneNote extends AppCompatActivity {

private TextView onenote_name;
private TextView onenote_desc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_note);
        getIntent().getIntExtra("noteid",0);
//        getIntent().getStringExtra("notedesc");
//        getIntent().getStringExtra("notename");
        getSupportActionBar().setTitle(getIntent().getStringExtra("notename"));
        onenote_name = findViewById(R.id.onenote_name);
        onenote_desc = findViewById(R.id.onenote_desc);

        onenote_name.setText( getIntent().getStringExtra("notename"));
        onenote_desc.setText( getIntent().getStringExtra("notedesc"));

    }
}
