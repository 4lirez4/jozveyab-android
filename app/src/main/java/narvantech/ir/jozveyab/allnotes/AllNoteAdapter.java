package narvantech.ir.jozveyab.allnotes;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import narvantech.ir.jozveyab.MainActivity;
import narvantech.ir.jozveyab.R;
import narvantech.ir.jozveyab.models.Jozve;
import narvantech.ir.jozveyab.updatenote.UpdateNoteActivity;
import narvantech.ir.jozveyab.utils.Constants;
import narvantech.ir.jozveyab.utils.Ret;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AllNoteAdapter extends RecyclerView.Adapter<AllNoteAdapter.NoteViewHolder> {

    private List<Jozve> notes;
    private Context context;

    public AllNoteAdapter(List<Jozve> notes, Context context) {
        //    if(this.notes==null){
        //       this.notes=new ArrayList<AllNotesModel>();
        //     }else {
        this.notes = notes;
        this.context = context;
        //    }
    }

    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemview = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.note_list_item, parent, false);
        return new NoteViewHolder(itemview);
    }

    @Override
    public void onBindViewHolder(NoteViewHolder holder, int position) {
        final Jozve jozve = notes.get(position);
        holder.tvName.setText(jozve.getName());
        holder.tvDescripton.setText(jozve.getDescription());
        holder.tvField.setText("تست");
        holder.tvUniversity.setText("تست");
        holder.notelist_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent singleNote = new Intent(context, OneNote.class);
                singleNote.putExtra("noteid", jozve.getId());
                singleNote.putExtra("notedesc", jozve.getDescription());
                singleNote.putExtra("notename", jozve.getName());
                context.startActivity(singleNote);
            }
        });

        holder.note_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, UpdateNoteActivity.class);
                intent.putExtra("id", jozve.getId());
                intent.putExtra("name", jozve.getName());
                intent.putExtra("desc", jozve.getDescription());
                context.startActivity(intent);
            }
        });


        holder.note_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeleteNote deleteNote= new DeleteNote();
                deleteNote.start(jozve.getId());

            }
        });
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    static class NoteViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private TextView tvDescripton;
        private TextView tvUniversity;
        private TextView tvField;
        private CardView notelist_container;
        private ImageView note_edit;
        private ImageView note_delete;
        //   private TextView tvDate;
        //   private RatingBar ratingBar;
        //  private ImageView imgNote;

        public NoteViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name_note_list);
            tvDescripton = itemView.findViewById(R.id.tv_description);
            tvUniversity = itemView.findViewById(R.id.tv_university);
            tvField = itemView.findViewById(R.id.tv_field);
            notelist_container = itemView.findViewById(R.id.notelist_container);
            note_edit = itemView.findViewById(R.id.note_edit);
            note_delete = itemView.findViewById(R.id.note_delete);
            //       tvDate =itemView.findViewById(R.id.tv_date_note_list);
            //       ratingBar=itemView.findViewById(R.id.ratingbar_list);
            //       imgNote=itemView.findViewById(R.id.img_note_list);
        }

    }


    private class DeleteNote implements Callback<ResponseBody> {
        public ResponseBody myData;

        public void start(int id) {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            Ret ret = retrofit.create(Ret.class);
            Call<ResponseBody> call = ret.deleteNote(id);
            call.enqueue(this);
        }

        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            if (response.isSuccessful()) {
                this.myData = response.body();
                Toast.makeText(context, "جزوه با موفقیت حذف شد", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t) {
            Log.d("delete", t.getMessage());
            Toast.makeText(context, "مشکل", Toast.LENGTH_SHORT).show();

        }
    }


}