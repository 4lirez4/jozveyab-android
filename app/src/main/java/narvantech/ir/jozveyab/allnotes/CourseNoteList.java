package narvantech.ir.jozveyab.allnotes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import narvantech.ir.jozveyab.MainActivity;
import narvantech.ir.jozveyab.R;
import narvantech.ir.jozveyab.models.Jozve;
import narvantech.ir.jozveyab.utils.Constants;
import narvantech.ir.jozveyab.utils.Ret;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CourseNoteList extends AppCompatActivity {
    RecyclerView rv_notes;
    List<AllNotesModel> noteList;
    AllNoteAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_note_list);
        rv_notes = findViewById(R.id.rv_notes);
        getSupportActionBar().setTitle(getIntent().getStringExtra("name"));

        GetNoteRet noteRet=new GetNoteRet();
        noteRet.start(getIntent().getIntExtra("id",1));

    }




    private class GetNoteRet implements Callback<List<Jozve>> {
        public List<Jozve> myData;

        public void start(int id) {
            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
            Ret ret = retrofit.create(Ret.class);
            Call<List<Jozve>> call = ret.getNoteByCourse(id);
            call.enqueue(this);
        }

        @Override
        public void onResponse(Call<List<Jozve>> call, Response<List<Jozve>> response) {
            if (response.isSuccessful()) {
                this.myData = response.body();
//                Toast.makeText(getApplicationContext(), "شما با موفقیت وارد شدید", Toast.LENGTH_SHORT).show();
                adapter = new AllNoteAdapter(myData,getApplicationContext());
                rv_notes.setLayoutManager(new LinearLayoutManager(CourseNoteList.this));
                rv_notes.setItemAnimator(new DefaultItemAnimator());
                rv_notes.setAdapter(adapter);
            Log.d("coursenote", response.raw().message());

            }
        }

        @Override
        public void onFailure(Call<List<Jozve>> call, Throwable t) {
            Log.d("coursenote", t.getMessage());
            Toast.makeText(getApplicationContext(), "مشکل", Toast.LENGTH_SHORT).show();

        }
    }



}
