package narvantech.ir.jozveyab;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import narvantech.ir.jozveyab.allnotes.OneNote;

public class TestActivity extends AppCompatActivity {


    Button btn1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        btn1=findViewById(R.id.btn_go_to_One_note);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TestActivity.this, OneNote.class);
                startActivity(intent);
            }
        });

    }
}
