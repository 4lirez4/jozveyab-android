package narvantech.ir.jozveyab.utils;

import android.app.Application;
import android.content.Context;
import android.support.v7.widget.AppCompatTextView;

import narvantech.ir.jozveyab.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Bezet on 06/04/2017.
 */

public class AppController extends Application {

    private static AppController mInstance;

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    @Override
    public void onCreate() {
        super.onCreate();


        mInstance = this;

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/iransans.ttf")
                .addCustomStyle(AppCompatTextView.class, android.R.attr.textViewStyle)
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

    }


    public static synchronized AppController getInstance() {
        return mInstance;
    }


}
