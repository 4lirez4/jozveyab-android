package narvantech.ir.jozveyab.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.widget.Toast;

import java.util.List;

/**
 * Created by sgerami on 1/10/18.
 */

public class SocialAppUtil {

    //TODO: NEEDS TESTING
    public static void openTelegramChannel(Context context, String channelUri) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(channelUri));
        final String appName = "org.telegram.messenger";
        i.setPackage(appName);
        context.startActivity(i);
    }

    public static void openTelegramChannel2(Context context, String channelName) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("tg://resolve?domain=" + channelName));
        Intent telegram = new Intent(Intent.ACTION_VIEW, Uri.parse("https://telegram.me/" + channelName));
        context.startActivity(intent);
    }

    /**
     * Indicates whether the specified app ins installed and can used as an intent. This
     * method checks the package manager for installed packages that can
     * respond to an intent with the specified app. If no suitable package is
     * found, this method returns false.
     *
     * @param context The application's environment.
     * @param appName The name of the package you want to check
     * @return True if app is installed
     */
    public static boolean isAppAvailable(Context context, String appName) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(appName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static void openInstagramPage(Context context, String pageUrl) {
        Uri uri = Uri.parse(pageUrl);
        Intent insta = new Intent(Intent.ACTION_VIEW, uri);
        insta.setPackage("com.instagram.android");

        if (isIntentAvailable(context, insta)) {
            context.startActivity(insta);
        } else {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(pageUrl.replace("_u/", ""))));
        }
    }

    private static boolean isIntentAvailable(Context ctx, Intent intent) {
        final PackageManager packageManager = ctx.getPackageManager();
        List<ResolveInfo> list
                = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    /**
     * Intent to send a telegram message
     *
     * @param msg
     */
    void intentMessageTelegram(Context context, String msg) {
        final String appName = "org.telegram.messenger";
        final boolean isAppInstalled = isAppAvailable(context, appName);
        if (isAppInstalled) {
            Intent myIntent = new Intent(Intent.ACTION_SEND);
            myIntent.setType("text/plain");
            myIntent.setPackage(appName);
            myIntent.putExtra(Intent.EXTRA_TEXT, msg);//
            context.startActivity(Intent.createChooser(myIntent, "Share with"));
        } else {
            Toast.makeText(context, "Telegram not Installed", Toast.LENGTH_SHORT).show();
        }
    }

}
