package narvantech.ir.jozveyab.utils;

import android.util.Log;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Sajjad Gerami on 13/02/2018 - 22:19.
 */

public class Settings {
    public static final String TAG = "AKKALA";
    public static final String PREFS = "AKKALA";
    public static final String INSTAGRAM_PAGE_URL = "http://instagram.com/_u/mimmarket";
    public static final String TELEGRAM_CHANNEL_URL = "https://telegram.me/mimmarket";
    public static final String TELEGRAM_CHANNEL_NAME = "mimmarket";
    public static final String SUPPORT_PHONE_NUMBER = "02537770118";
    //    public static String BASE_URL = "http://192.168.90.11:8054/app/";
//    public static String BASE_URL = "http://37.59.145.138:8054/app/";
    public static final String BASE_URL = "http://mimmarket.com/app/";
    public static final String BASE_IMG_URL = BASE_URL + "image/";
    public static final int STATE_ID = 96; //qom
    public static final long SLIDER_DURATION = 4000L;
    public static ArrayList<String> imageGallery = new ArrayList<>();
    public static ArrayList<String> galleryImage = new ArrayList<>();
    //This field should be updated on login
    public static String KEY;
    public static boolean LOGGED_IN = false;

    public static void generateKey() {
        String uuid = UUID.randomUUID().toString();
        long millis = System.currentTimeMillis();
        Settings.KEY = uuid + millis;
        Log.d(TAG, "first run. key=" + Settings.KEY);
//        Toast.makeText(this, "Key Generated" + Settings.KEY, Toast.LENGTH_SHORT).show();
    }
}
