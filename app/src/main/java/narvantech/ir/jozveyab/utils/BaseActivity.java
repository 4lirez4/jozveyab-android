package narvantech.ir.jozveyab.utils;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by alireza on 2/21/18.
 */

public class BaseActivity extends AppCompatActivity {
  //  @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //rtl the activity layout - drawer etc.
    //    getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
