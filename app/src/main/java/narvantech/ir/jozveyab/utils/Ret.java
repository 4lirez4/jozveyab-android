package narvantech.ir.jozveyab.utils;


import java.util.List;

import narvantech.ir.jozveyab.models.Jozve;
import narvantech.ir.jozveyab.models.Users;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by alireza on 9/23/17.
 */

public interface Ret {

    //jozve
    @GET("jozve/")
    Call<List<Jozve>> getAllNote();

//    @GET("jozve/{id}")
//    Call<List<Jozve>> getNoteById(@Path("id") int id);

    @GET("jozve/category/{id}")
    Call<List<Jozve>> getNoteByCourse(@Path("id") int id);

    @GET("jozve/post")
    Call<ResponseBody> postNote(@Query("name") String name, @Query("link") String link, @Query("description") String description);

    @GET("jozve/update")
    Call<ResponseBody> updateNote(@Query("id") int id, @Query("name") String name, @Query("description") String description);

    @GET("jozve/delete")
    Call<ResponseBody> deleteNote(@Query("id") int id);


    //user

    @GET("users/signup")
    Call<ResponseBody> signUp(@Query("fname") String fname, @Query("lname") String lname, @Query("email") String email, @Query("password") String password);

    @GET("users")
    Call<Users> signIn(@Query("email") String email, @Query("password") String password);

}
